// buat bilangan prima 1 sampai dengan 100
// bilangan prima adalah bilangan yang hanya bisa dibagi oleh bilangan itu sendiri dan satu
function prima(start = 1, stop = 100) {
  let result = []
  let bilangan = start;
  for (bilangan; bilangan <= stop; bilangan++) {
    let totalPembagi = 0;
    for (let x = 1; x <= bilangan; x++) {
      if (bilangan % x == 0) {
        totalPembagi++;
      }
    }
    if (totalPembagi == 2) {
      result.push(bilangan);
    }
  }
  return result;
}

console.log(...prima(1, 100))

function piramidSatu(num) {
  let str = "";
  for (let i = 0; i < num; i++) {
    let a = "";
    for (let j = 0; j <= i; j++) {
      a += "*";
    }
    a += "\n"
    str += a;
  }
  return str;
}

function piramidDua(num) {
  let str = "";
  for (let i = 0; i < num; i++) {
    let a = "";
    for (let z = i; z < num; z++) {
      a += "*";
    }
    for (let j = 0; j <= i; j++) {
      a += " ";
    }
    a += "\n"
    str += a;
  }
  return str;
}

function piramidTiga(num) {
  // 0, 1, 2, 3
  // 1(3-3), 3(2-2), 5(1-1), 7(0-0)
  let str = "";
  let deretGanjil = [];
  let pointer = 0;
  while (deretGanjil.length != num) {
    if (pointer % 2 != 0) {
      deretGanjil.push(pointer);
    }
    pointer++
  }
  for (let i = 0; i < num; i++) {
    let a = "";
    let space = num - (i + 1);
    // add space before
    for (let y = 1; y <= space; y++) {
      a += " ";
    }
    // add piramid
    for (let c = 0; c < deretGanjil[i]; c++) {
      // a += "\u2665";
      a += "*";
    }
    // add space after
    for (let z = 1; z <= space; z++) {
      a += " ";
    }
    a += "\n"
    str += a;
  }
  return str;
}

function piramidEmpat(num) {
  // 0, 1, 2, 3
  // 1(3-3), 3(2-2), 5(1-1), 7(0-0)
  let str = "";
  let deretGenap = [];
  let pointer = 0;
  while (deretGenap.length != num) {
    if (pointer % 2 == 0) {
      deretGenap.push(pointer);
    }
    pointer++
  }
  deretGenap = deretGenap.reverse();
  index = deretGenap.indexOf(0);
  deretGenap[index] = 1;
  for (let i = 0; i < num; i++) {
    let a = "";
    let space = i;
    // add space before
    for (let y = 1; y <= space; y++) {
      if (deretGenap[i] == 1) {
        y++
      }
      a += " ";
    }
    // add piramid
    for (let c = 0; c < deretGenap[i]; c++) {
      a += "*";
    }
    // add space after
    for (let z = 1; z <= space; z++) {
      a += " ";
    }
    a += "\n"
    str += a;
  }
  return str;
}

console.log(piramidSatu(4))
console.log(piramidDua(4))
console.log(piramidTiga(4))
console.log(piramidEmpat(4))

const data = [
  {
    "nama": "Andi",
    "nilai": 90,
  },
  {
    "nama": "Budi",
    "nilai": 89,
  },
  {
    "nama": "Santi",
    "nilai": 70,
  },
  {
    "nama": "Yuni",
    "nilai": 60,
  },
];

function sumNilai(data) {
  let total = 0;
  data.forEach((item, index) => total += item.nilai);
  // console.log(total)
  result = total / data.length;
  return result;
}

console.log(sumNilai(data))